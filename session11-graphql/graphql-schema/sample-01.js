/*
**   GraphQL: Sample Service
**  
**
**  Stage of Extension:
**  01. just plain all-in-one GraphQL "Hello World"
*/

import util              from "util"
import * as GraphQL      from "graphql"

var helloWorldQuery = new GraphQL.GraphQLObjectType({
    name: "Root",
    fields: () => ({
        "Hello": {
            type: GraphQL.GraphQLString,
            resolve: () => { return "World" }
        }
    })
})

var helloSchema = new GraphQL.GraphQLSchema({
    query: helloWorldQuery
})
GraphQL.graphql(
    helloSchema,
    `query { Hello }`,
    null, null, {}
).then((result) => {
    console.log("OK", util.inspect(result, { depth: null, colors: true }))
}).catch((result) => {
    console.log("ERROR", result)
})

