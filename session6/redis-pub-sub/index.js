const redis = require('redis');
const redisClient = redis.createClient({
    host: 'redis-14987.c57.us-east-1-4.ec2.cloud.redislabs.com',
    port: 14987
});
redisClient.AUTH("bdap6uQaV37A93V2tWpjbwHkmZcqXWau");

redisClient.on('error', (err) => {
    console.log('Redis error: ', err);
});

redisClient.set("random_key", "some great value", function (err, reply) {
    // This will either result in an error (flush parameter is set to true)
    // or will silently fail and this callback will not be called at all (flush set to false)
    console.log(err);
});
//redisClient.end(true); // No further commands will be processed
redisClient.get("random_key", function (err, reply) {
    console.log("cached value for  random_key is", reply);
    console.log(err); // => 'The connection has already been closed.'
});

//var publisher = redis.createClient();
var msg = "Hello TGA";
redisClient.publish('notification', msg, function () {
    console.log("Published the message")
});