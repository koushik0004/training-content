const WebSocket = require('ws')

const wss = new WebSocket.Server({ port: 8080 })

wss.on('connection', (ws) => {

  ws.on('message', (message) => {
    console.log(`Received message => ${message}`)
    ws.send("You had send:" + message);
  })

  ws.send('Hello Web sockets!');

  setInterval(
    () => {
        console.log("sending message");
        ws.send('time is now :' + new Date().getTime());
    },
    3000
  );
  
})