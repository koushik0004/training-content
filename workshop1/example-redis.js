const express = require('express');
const redis = require('redis');
const fetch = require("node-fetch");
var process = require('process');
const redisClient = redis.createClient({
    host: 'redis-14987.c57.us-east-1-4.ec2.cloud.redislabs.com',
    port: 14987
});
redisClient.AUTH("bdap6uQaV37A93V2tWpjbwHkmZcqXWau");

const app = express();

redisClient.on('error', (err) => {
    console.log('Redis error: ', err);
});

redisClient.set("random_key", "some great value", function (err, reply) {
    // This will either result in an error (flush parameter is set to true)
    // or will silently fail and this callback will not be called at all (flush set to false)
    console.log(err);
});
//redisClient.end(true); // No further commands will be processed
redisClient.get("random_key", function (err, reply) {
    console.log("cached value for  random_key is", reply);
    console.log(err); // => 'The connection has already been closed.'
});


app.get('/', function (req, res, next) {

    redisClient.get("random_key1", function (err, reply) {
        if (reply != null) {
            console.log("reply for random_key is", reply);
            return res.end('welcome to the redis demo!, you saved the value for random_key in in redis ' + reply);
        } else {
            redisClient.set("random_key1", "some great value");
            console.log(err); // => 'The connection has already been closed.'
            return res.end('welcome to the redis demo!, you need to save the value for random_key in in redis ');

        }

    });

})

// start express server at 3000 port
app.listen(3000, () => {
    console.log('Server with process id ' + process.pid + 'listening on port: ', 3000)
});