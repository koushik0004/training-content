const config = require('config.json');
const mongoose = require('mongoose');
mongoose.connect(process.env.MONGODB_URI || config.connectionString, {useNewUrlParser: true })
.then(() => {
    console.log('Connection to database has been established successfully.');
  }).catch(err => {
    console.log('Unable to connect to the database:', err);
  });
mongoose.Promise = global.Promise;

mongoose.connection.on('error', err => {
  console.log('Unable to connect to the database error:', err);
  });

module.exports = {
    User: require('../users/user.model')
};