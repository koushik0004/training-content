var createError = require('http-errors');
require('envdotjson').load();
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
//var logger = require('morgan');
const { connectToDB } = require('./utils/db');
const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');

const swaggerSpec = YAML.load('./swagger/swagger.yaml');


var indexRouter = require('./routes/index');

const PORT = process.env.PORT || 3000;

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
//app.use(logger('dev'));
app.use(express.json());
// app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

connectToDB()
  .then(() => {
     //logger.info('DB connected');
     console.log("DB connected");
  })
  .catch(e => {
    //logger.info('DB connection failed');
    console.log("DB connection failed", e);
    throw new Error(e);
  });

module.exports = app;
